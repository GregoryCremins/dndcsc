import eyed3
import glob
import wx

class Mp3Panel(wx.Panel):
    def __init__(self, parent):
        super().__init__(parent)
        main_sizer = wx.BoxSizer(wx.VERTICAL)
        self.row_obj_dict = {}

        self.list_ctrl = wx.ListCtrl(
            self, size=(-1, 100),
            style=wx.LC_REPORT | wx.BORDER_SUNKEN
        )
        self.list_ctrl.InsertColumn(0, 'Name', width=200)
        self.list_ctrl.InsertColumn(1, 'Level', width=40)
        self.list_ctrl.InsertColumn(2, 'Race', width=200)
        self.list_ctrl.InsertColumn(3, 'Class', width=200)
        main_sizer.Add(self.list_ctrl, 0, wx.ALL | wx.EXPAND, 5)
        edit_button = wx.Button(self, label='Edit')
        edit_button.Bind(wx.EVT_BUTTON, self.on_edit)
        main_sizer.Add(edit_button, 0, wx.ALL | wx.CENTER, 5)
        self.SetSizer(main_sizer)

    def on_edit(self, event):
        print('in on_edit')

    def update_mp3_listing(self, folder_path):
        print(folder_path)
        for filename in os.listdir(path):
          if not filename.endswith('.xml'): continue
          fullname = os.path.join(path, filename)
          tree = ET.parse(fullname)
